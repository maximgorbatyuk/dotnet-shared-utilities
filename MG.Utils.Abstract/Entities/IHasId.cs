﻿namespace MG.Utils.Abstract.Entities;

public interface IHasId : IHasIdBase<long>
{
}